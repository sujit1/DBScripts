var express = require('express'),
    mysql = require('mysql'),
    app = express(),
    https = require('https'),
    bodyParser = require('body-parser'),
    port = process.env.PORT || 8080,
    Promise = require('promise');
app.set('views', __dirname + '/Views');
app.engine('html', require('ejs').__express);
app.set('view engine', 'ejs');



app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));
app.use(bodyParser.json({
    limit: '50mb'
}));








var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "blaze",
    database: "IRDatabase"
});

con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");

    app.listen(port, function () {

        console.log('server connected');


    });

});


app.get("/getIRBrands", function (req, res) {

    var sql = "Select * from tbBrandId";
    con.query(sql, function (err, result) {
        if (err) throw err;
        if (result.length > 0) {
            res.send({
                data: result
            });
        } else {
            res.send("no data found");
        }
    });



});

app.post("/getIRData", function (req, res) {

    if (req.body && req.body.function_id) {
        var promise = [];
        switch (req.body.function_id) {
            case "3":
                var sql = "Select * from tbBrandId";
                con.query(sql, function (err, result) {
                    if (err) throw err;
                    if (result.length > 0) {
                        // res.send({
                        //     data: result
                        // });

                        var brandName = {};
                        var brandNameCN = {};
                        var brandNameZH = {};
                        var brandNameJP = {};
                        var isManuf = {};
                        var objbrandName = {};
                        var objbrandNameCN = {};
                        var objbrandNameZH = {};
                        var objbrandNameJP = {};
                        var objisManuf = {};
                        for (var i = 0; i < result.length; i++) {

                            objbrandName[result[i].brandId] = result[i].brandName;
                            objbrandNameCN[result[i].brandId] = result[i].brandNameCN;
                            objbrandNameZH[result[i].brandId] = result[i].brandNameZH;
                            objbrandNameJP[result[i].brandId] = result[i].brandNameJP;
                            objisManuf[result[i].brandId] = result[i].isManuf;


                            //promise.push(obj);
                        }
                        //console.log("values of obje is "+JSON.stringify(promise));
                        brandName.brandName = objbrandName;
                        brandName.brandNameCN = objbrandNameCN;
                        brandName.brandNameZH = objbrandNameZH;
                        brandName.brandNameJP = objbrandNameJP;
                        brandName.isManuf = objisManuf;

                        res.send({

                            "result_code": "1",
                            "result_data": brandName,
                            "result_des": "Request Processed"

                        })


                    } else {
                        res.send("no data found");
                    }
                });

                break;
            case "5":
                if (req.body.brandId) {
                    //console.log("data result " + JSON.stringify(req.body));
                    var sql = "Select * from tbCodelist_AC where brandId=" + req.body.brandId;
                    con.query(sql, function (err, result) {
                        //console.log("data result " + JSON.stringify(result));
                        if (result.length > 0) {
                            var ModelName = {};
                            var codeNum = {};
                            var rank = {};
                            var year = {}
                            objcodeNum = {};
                            objrank = {};
                            objyear = {};
                            for (var j = 0; j < result.length; j++) {
                                objcodeNum[j] = result[j].codeNum;
                                objrank[j] = result[j].rank1;
                                objyear[j] = result[j].year;
                            }

                            ModelName.codeNum = objcodeNum;
                            ModelName.rank = objrank;
                            ModelName.year = objyear;

                            res.send({

                                "result_code": "1",
                                "result_data": ModelName,
                                "result_des": "Request Processed"

                            })
                        } else {
                            res.send("no data found for the given brand id");
                        }

                    });

                } else {
                    res.send({
                        "status": 0,
                        "message": "required parameters are missing"
                    });

                }
                break;
            case "502":

                if (req.body.codeNum) {

                    var sql = "Select * from tbIrData_AC where codeNum=" + req.body.codeNum;
                    con.query(sql, function (err, result) {

                        if(result.length>0)
                        {   var irDataModel={};
                            var AC = {};
                            var FunctionID = {};
                            var irData = {};
                            var dataID = {}
                            objFunctionID = {};
                            objirData = {};
                            objdataID = {};
                            for(var k=0;k<result.length;k++){
                                objFunctionID[k] = result[k].FunctionID;
                                objirData[k] = result[k].irData;
                                objdataID[k] = result[k].dataID;
                            }
                                AC.FunctionID=objFunctionID;
                                AC.dataID=objdataID;
                                AC.irData=objirData;
                                irDataModel.AC=AC;
                            res.send({

                                "result_code": "1",
                                "result_data": irDataModel,
                                "result_des": "Request Processed"

                            })
                        }
                        else
                        {
                            res.send("no data found for the given code number");
                        }
                    });


                } else {
                    res.send({
                        "status": 0,
                        "message": "required parameters are missing"
                    });


                }
                break;
             case "2":

             res.send({

                "result_code": "1",
                "result_data": {"Comment":{  "0":"AIR CONDITIONER"} ,"deviceName15" : { "0" : "AC"},"deviceName30" : { "0" = "AIR CONDITIONER"},"deviceNameCN" : {"0" : "\U7a7a\U8c03"}},
                "result_des": "Request Processed"

            })
               
             break;   
            default:
                res.send({
                    "status": 0,
                    "message": "different function_id need to implement "
                });
                break;
        }


    } else {
        res.send({
            "status": 0,
            "message": "required parameters are missing"
        });
    }



});
