#!/bin/bash
#DB Details
IP=`curl icanhazip.com`
USERNAME="root"
PASSWORD="blaze"
HOST="localhost"
S3BUCKET="vlebucket"
ServerName="B-one-prod-DB"
BACKUPNAME="Blaze_mysqlDB_backup"
Doubt_Subject="Backup -Success *Check Data Size*"
OutputFilesLocation="/home/ubuntu/Scripts"

#Backups storage path.
BACKUPLOCATION="/home/ubuntu/mysqlbackups"
emails="sridhar@blazeautomation.com sujit@blazeautomation.com" #multiple emails space separated
Down_Subject=" Backup -Failed"
Good_Subject=" Backup -Success"

#Current day
TODAY=$(date "+%Y-%m-%d")

#create folder today
mkdir -p $BACKUPLOCATION
cd $BACKUPLOCATION
#Full Backup Mode

/usr/bin/mysqldump -u $USERNAME -p$PASSWORD -h $HOST --master-data=2 --lock-all-tables  --all-databases | gzip -c >$BACKUPLOCATION/$BACKUPNAME$TODAY.sql.gz


#TO take a backup in .sql format
#/usr/bin/mysqldump -u $USERNAME -p$PASSWORD -h $HOST --lock-all-tables  --all-databases >$BACKUPLOCATION/$TODAY/project_name$TODAY.sql


#To grep from gz
#zcat /$BACKUPLOCATION/$BACKUPNAME$TODAY.sql.gz |grep 'Dump completed on' |cut -d' ' -f5-7 >$OutputFilesLocation/output.txt
#COMPLETEDTIME=$(<$OutputFilesLocation/output.txt)
#TimeCompare=`cat $OutputFilesLocation/output.txt |gawk '{print $1}'`
TimeCompare=$(echo $#)
#Compress Process
cd $BACKUPLOCATION
tar -czf $BACKUPNAME$TODAY.tar.gz $BACKUPNAME$TODAY.sql.gz

#Removing .sql.gz today files

rm -rf $BACKUPNAME$TODAY.sql.gz
#Useful Tips
#To decompress use tar -xvf do not use -xvzf, Sometimes size remains the same when compressed also
#To decompress sql.gz file use gunzip filename

#Logs Purge Process Comment it if you do not need it
#mysql -u root -password -e"PURGE BINARY LOGS BEFORE '$COMPLETEDTIME'";


#Automated SCP
###scp db file to remote server #########################
#/home/ubuntu/Scripts/scpscript.sh  /home/ubuntu/mysqlbackups/`date "+%Y-%m-%d"`.tar.gz
####################################################################################

#s3cmd put -f $BACKUPLOCATION/$BACKUPNAME`date "+%Y-%m-%d"`.tar.gz  s3://${S3BUCKET}/
s3cmd put $BACKUPLOCATION/$BACKUPNAME`date "+%Y-%m-%d"`.tar.gz  s3://${S3BUCKET}/
#Collecting and Saving Stats
#s3cmd ls s3://$S3BUCKET |grep -i "$BACKUPNAME`date "+%Y-%m-%d"`.tar.gz" |gawk '{print $1}' >$OutputFilesLocation/S3Date.txt
s3cmd ls s3://$S3BUCKET |grep -i "$BACKUPNAME`date "+%Y-%m-%d"`.tar.gz" |gawk '{print $1}' >$OutputFilesLocation/S3Date.txt
s3cmd info s3://$S3BUCKET/$BACKUPNAME`date "+%Y-%m-%d"`.tar.gz | sed -n '3 p' | gawk '{print $7}' >$OutputFilesLocation/S3Time.txt
s3cmd ls s3://$S3BUCKET |grep -i "$BACKUPNAME`date "+%Y-%m-%d"`.tar.gz" |gawk '{print $3}' >$OutputFilesLocation/S3TodaySize.txt
s3cmd ls s3://$S3BUCKET |grep -i "$BACKUPNAME`date -d "yesterday" +%F`.tar.gz"|gawk '{print $3}' >$OutputFilesLocation/S3YesterdaySize.txt
S3Time=$(<$OutputFilesLocation/S3Time.txt)
S3Date=$(<$OutputFilesLocation/S3Date.txt)
S3TodaySize=$(<$OutputFilesLocation/S3TodaySize.txt)
S3YesterdaySize=$(<$OutputFilesLocation/S3YesterdaySize.txt)
#Sends mail 
if [ $TimeCompare = 0 ]; then
Backup_Status="Backup on $ServerName IP $IP was completed, \n Details are :\n Backup Completed Time: $COMPLETEDTIME,\n Backup Moved to S3: $S3Date $S3Time,\n Data Size Yesterday: $S3YesterdaySize,\n Data Size Today: $S3TodaySize.\n"
status=1
fi

if [ $TimeCompare != 0 ]; then
Backup_error="Backup on $ServerName IP $IP is failed.\n Last Successful completetion on $COMPLETEDTIME\n"
status=2
fi

#if [ "$TimeCompare" = "$TODAY" && "$S3TodaySize" -le "$S3YesterdaySize" ]; then
#Backup_Msg="Backup on $ServerName IP $IP is success.\nThe data size is same or lesser that yesterday $COMPLETEDTIME.\nPlease Conform whether any data was deleted or there is no data since last backup. \n"
#Backup_Msg="Backup on $ServerName IP $IP was completed, \n Details are :\n Backup Completed Time: $COMPLETEDTIME,\n Backup Moved to S3: $S3Date $S3Time,\n Data Size Yesterday: $S3YesterdaySize,\n Data Size Today: $S3TodaySize.\nThe data size is same or lesser that yesterday $COMPLETEDTIME.\nPlease Conform whether any data was deleted or there is no data since last backup. \n"
#status=3
#fi
#Sending mails

if [ $status = 1 ]; then
for address in $emails; do
echo -e $Backup_Status | mail -s "$Good_Subject" $address
echo "Backup Success, sent email to $address"
done
fi

if [ $status = 2 ]; then
for address in $emails; do
echo -e $Backup_error | mail -s "$Down_Subject" $address
echo "Backup Failed, sent email to $address"
done
fi

                                                    
if [ $status = 3 ]; then
for address in $emails; do
echo -e $Backup_Msg | mail -s "$Doubt_Subject" $address
echo "Backup Success,But there was a difference in data size sent email to $address"
done
fi
