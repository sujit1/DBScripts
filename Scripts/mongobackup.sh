#!/bin/bash
#Write lock will be releases at the end of the script after completing backup process.



#mongo admin --eval "printjson(db.fsyncLock())"




#Paths And Credentials


MONGODUMP_PATH="/usr/bin/mongodump"
MONGO_HOST="127.0.0.1" # Always replace with your server ip
MONGO_PORT="27017"
S3_BUCKET_NAME="vlebucket" #replace with your bucket name on Amazon S3
backup_location="/home/ubuntu/mongobackups"
backup_name="Blaze_mongo_backup"
TODAY=$(date "+%Y-%m-%d")
EMAIL="sujit@blazeautomation.com sridhar@blazeautomation.com"


#Backup Process
cd $backup_location
$MONGODUMP_PATH  --host $MONGO_HOST --out $backup_name-$TODAY
tar cfz $backup_name-$TODAY.tar.gz $backup_name-$TODAY


CompletedTime=$(date +%Y-%m-%d" "%H:%M:%S)
echo $CompletedTime
#Unlock database writes
#mongo admin --eval "printjson(db.fsyncUnlock())"


rm -rf $backup_name-$TODAY
res=$(echo $?)

# Upload to S3
s3cmd put /$backup_location/$backup_name-$TODAY.tar.gz s3://${S3_BUCKET_NAME}/

#send mail
if [ "$res" = "0" ] ;
then
echo "Blaze Mongo Backup Success on $TODAY" | mail -s "Backup Success" $EMAIL
else
echo "Blaze Mongo Backup Failed on $TODAY" | mail -s "Backup Failed" $EMAIL
fi

